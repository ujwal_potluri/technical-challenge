using System.Collections.Generic;
using Search.Data.Models;

namespace Search.Business
{
    public interface ISearchService
    {
        IEnumerable<Title> SearchText(string title);
        Title GetTitle(string name);
    }
}