﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Search.Data;
using Search.Data.Models;

namespace Search.Business
{
    public class SearchService : ISearchService
    {
        private readonly ISearchRepository _searchRepository;

        public SearchService()
            : this(new SearchRepository())
        {

        }

        public SearchService(ISearchRepository searchRepository)
        {
            _searchRepository = searchRepository;
        }

        public IEnumerable<Title> SearchText(string title)
        {
            var dataResult = _searchRepository.FindAllByTitle(title);
            return dataResult;
        }

        public Title GetTitle(string name)
        {
            var title = _searchRepository.GetTitleByName(name);
            return title;
        }
    }
}
