﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Search.Data.Models
{
    [Table("Title") ]
    public class Title
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string TitleNameSortable { get; set; }
        public int ReleaseYear { get; set; } 
    }
}
