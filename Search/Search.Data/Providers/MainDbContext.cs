﻿
using System.Data.Entity;
using Search.Data.Models;

namespace Search.Data.Providers
{
    public class MainDbContext : DbContext
    {
        public MainDbContext()
            : base("TitlesEntities")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Title>().ToTable("Title");
        }
    }

}
