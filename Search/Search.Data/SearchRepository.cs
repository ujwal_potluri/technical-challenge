﻿using System.Collections.Generic;
using System.Linq;
using NinjaNye.SearchExtensions.Fluent;
using Search.Data.Models;
using Search.Data.Providers;

namespace Search.Data
{
    public interface ISearchRepository
    {
        IEnumerable<Title> FindAllByTitle(string title);
        Title GetTitleByName(string name);
    }


    public class SearchRepository : ISearchRepository
    {
        public IEnumerable<Title> FindAllByTitle(string title)
        {
            using (var context = new MainDbContext())
            {
                string[] searchTerms = title.Split(' ');
                var result =
                    context.Set<Title>().Search(x => x.TitleName, x => x.TitleNameSortable).Containing(searchTerms);

                return result.ToList();
            }
        }

        public Title GetTitleByName(string name)
        {
            using (var context = new MainDbContext())
            {
                var title = context.Set<Title>().SingleOrDefault(t => t.TitleName == name);
                return title;
            }
        }
    }
}
