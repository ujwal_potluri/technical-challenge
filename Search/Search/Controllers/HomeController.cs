﻿using System.Web.Mvc;
using Search.Business;

namespace Search.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISearchService _searchService;
        public HomeController()
            : this(new SearchService())
        { }

        public HomeController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Search(string searchParameter)
        {
            if (string.IsNullOrEmpty(searchParameter))
                return Json(new { Success = false, ErroMessage = "Search parameter cannot be empty" });

            var result = _searchService.SearchText(searchParameter);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(string name)
        {
            var title = _searchService.GetTitle(name);
            return View("Details", title);
        }
    }
}
