﻿namespace Search.Data.Providers
{
    public interface IDbContextProvider
    {
        MainDbContext GetMainContext();
    }

    public class DbContextProvider : IDbContextProvider
    {
        public MainDbContext GetMainContext()
        {
            return new MainDbContext();
        }
    }
}
